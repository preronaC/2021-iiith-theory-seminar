\documentclass[10pt,dvipsnames,aspectratio=169]{beamer}

\usetheme[progressbar=foot]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{commons}
\include{macros}

\usepackage{tikz}

\usetikzlibrary{shapes,arrows,calc,patterns,positioning}
\usetikzlibrary{decorations.pathmorphing}

\title{Lower Bounds in Algebraic Circuit Complexity}
%\subtitle{}

\author{\textbf{Prerona Chatterjee}}
\date{September 1, 2021}
\institute{Tata Institute of Fundamental Research, Mumbai}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 2em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\tikzset{set/.style={draw,circle,inner sep=0pt,align=center}}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\begin{frame}{Polynomials}

	\vspace{1.5em}
	\begin{center}
		Polynomials need to be computed frequently as sub-routines of various algorithms.
	\end{center}\pause

	\vspace{.5em}
	\begin{columns}
		\begin{column}{.4\textwidth}
			\[
				\det
				\inparen{
					\begin{bmatrix}
						x_{11} & x_{12} & \cdots & x_{1n}\\
						x_{21} & x_{22} & \cdots & x_{2n}\\
						\vdots & \vdots & \ddots & \vdots\\
						x_{n1} & x_{n2} & \cdots & x_{nn}
					\end{bmatrix}
				}
			\]
			\begin{center}
				\[
					\Det{n}(\vecx) = \sum_{\sigma \in S_n} (-1)^{\sgn(\sigma)} \prod_{i=1}^{n} x_{i \sigma(i)}	
				\]
			\end{center}
		\end{column}\pause
		\begin{column}{.6\textwidth}
		\[
			\begin{bmatrix}
				x^{(1)}_{11} & x^{(1)}_{12} & \cdots & x^{(1)}_{1n}\\
				x^{(1)}_{21} & x^{(1)}_{22} & \cdots & x^{(1)}_{2n}\\
				\vdots & \vdots & \ddots & \vdots\\
				x^{(1)}_{n1} & x^{(1)}_{n2} & \cdots & x^{(1)}_{nn}
			\end{bmatrix}
			\times
			\cdots
			\times
			\begin{bmatrix}
				x^{(d)}_{11} & x^{(d)}_{12} & \cdots & x^{(d)}_{1n}\\
				x^{(d)}_{21} & x^{(d)}_{22} & \cdots & x^{(d)}_{2n}\\
				\vdots & \vdots & \ddots & \vdots\\
				x^{(d)}_{n1} & x^{(d)}_{n2} & \cdots & x^{(d)}_{nn}
			\end{bmatrix}
		\]

		\vspace{-1.5em}
		\begin{center}
			\[
				\imm{n}{d}(\vecx) = \sum_{k_0, k_d = 1}^{n} \sum_{k_1, \ldots, k_{d-1} = 1}^{n} \prod_{i=1}^{d} x^{(i)}_{k_{i-1} k_i}
			\]
		\end{center}
		\end{column}
	\end{columns}\pause
	\vspace{1.5em}
	\begin{center}
		Can the given polynomial be computed \textcolor{MidnightBlue}{efficiently}?
	\end{center}
\end{frame}

\begin{frame}{Algebraic Models of Computation}
	\begin{columns}
		\begin{column}{.4\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=2cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
				node [sum] at ($(top)+(-1.5,-3)$) (sum1) {\suma}
				node [sum] at ($(top)+(0,-3)$) (sum2) {\suma}
				node [sum] at ($(top)+(1.5,-3)$) (sum3) {\suma}
				node [sum] at ($(top)+(-1.5,-4.5)$) (var1) {$x_1$}
				node [sum] at ($(top)+(0,-4.5)$) (var2) {$x_2$}
				node [sum] at ($(top)+(1.5,-4.5)$) (var3) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum1) -- (mult2);
				\draw[->](sum2) -- (mult2);
				\draw[->](sum3) -- (mult1);
				\draw[->](var1) -- (sum1);
				\draw[->](var1) -- (sum2);
				\draw[->](var2) -- (sum1);
				\draw[->](var2) -- (sum3);
				\draw[->](var3) -- (sum2);  
				\draw[->](var3) -- (sum3); 
				
				\node at (2.5,.8) {$\ckt$};
			\end{tikzpicture}
		\end{center}
		\end{column}\pause
		\begin{column}{.6\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=1cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
				node [sum] at ($(top)+(-2.25,-3)$) (sum1) {\suma}
				node [sum] at ($(top)+(-.75,-3)$) (sum2) {\suma}
				node [sum] at ($(top)+(0.75,-3)$) (sum3) {\suma}
				node [sum] at ($(top)+(2.25,-3)$) (sum4) {\suma};
				\node (var1) at ($(top)+(-2.75,-4.5)$) {$x_1$};
				\node (var2) at ($(top)+(-2,-4.5)$) {$x_2$};
				\node (var3) at ($(top)+(-1.25,-4.5)$) {$x_2$};
				\node (var4) at ($(top)+(-.5,-4.5)$) {$x_3$};
				\node (var5) at ($(top)+(0.5,-4.5)$) {$x_1$};
				\node (var6) at ($(top)+(1.25,-4.5)$) {$x_2$};
				\node (var7) at ($(top)+(2,-4.5)$) {$x_1$};
				\node (var8) at ($(top)+(2.75,-4.5)$) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum2) -- (mult1);
				\draw[->](sum3) -- (mult2);
				\draw[->](sum4) -- (mult2);
				\draw[->](var1) -- (sum1);
				\draw[->](var2) -- (sum1);
				\draw[->](var3) -- (sum2);
				\draw[->](var4) -- (sum2);
				\draw[->](var5) -- (sum3);
				\draw[->](var6) -- (sum3);
				\draw[->](var7) -- (sum4);
				\draw[->](var8) -- (sum4);   
				
				\node at (1.5,.8) {$\form$};
			\end{tikzpicture}
		\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Algebraic Branching Programs}
	\begin{center}
	\begin{tikzpicture}[thick, node distance=2cm]
		\hspace{-.5em}
		\node[circle, draw=black] (start) {$s$};
		\node[circle, draw=black] (l11) at ($(start)+(2.6,1.5)$) {};
		\node[circle, draw=black] (l12) at ($(start)+(2.6,-.5)$) {};
		\node[circle, draw=black] (l21) at ($(start)+(5.2,0)$) {};
		\node[circle, draw=black] (l31) at ($(start)+(7.8,1.5)$) {};
		\node[circle, draw=black] (l32) at ($(start)+(7.8,-.5)$) {};
		\node[circle, draw=black] (l41) at ($(start)+(10.4,1.5)$) {};
		\node[circle, draw=black] (l42) at ($(start)+(10.4,-.5)$) {};
		\node[circle, draw=black] (end) at ($(start)+(13,1)$) {t};
		
		\draw[->](start) -- (l11);
		\only<2-4>{\draw[->](start) -- node[left] {\small{$(2x+3)$}} (l11)};
		\only<3>{\textcolor{red}{\draw[->](start) -- node[left] {\small{\textcolor{black}{$(2x+3)$}}} (l11);}}
		\draw[->](start) -- (l12);
		\draw[->](l11) -- (l21);
		\only<2-4>{\draw[->](l11) -- node[left] {\small{$(x+3y)$}} (l21)};
		\only<3>{\textcolor{red}{\draw[->](l11) -- node[left] {\small{\textcolor{black}{$(x+3y)$}}} (l21);}}
		\draw[->](l12) -- (l21);
		\draw[->](l21) -- (l31);
		\only<2-4>{\draw[->](l21) -- node[left] {\small{$(y+5)$}} (l31)};
		\only<3>{\textcolor{red}{\draw[->](l21) -- node[left] {\small{\textcolor{black}{$(y+5)$}}}(l31);}}
		\draw[->](l21) -- (l32);
		\draw[->](l31) -- (l42);
		\only<2,4>{\draw[->](l31) -- node[left] {\small{$10$}} (l42)};
		\only<3>{\textcolor{red}{\draw[->](l31) -- node[left] {\small{\textcolor{black}{$10$}}}(l42);}}
		\draw[->](l32) -- (l41);
		\draw[->](l32) -- (l42);
		\draw[->](l41) -- (end);
		\draw[->](l42) -- (end);
		\only<2-4>{\draw[->](l42) -- node[left] {\small{$(x+y+7)$}} (end)};
		\only<3>{\textcolor{red}{\draw[->](l42) -- node[left] {\small{\textcolor{black}{$(x+y+7)$}}} (end);}}

		\node at ($(start)+(13,-.25)$) {$\abp$};
	\end{tikzpicture}
	\end{center}\pause

	\vspace{1.5em}
	\begin{itemize}
		\item Label on each edge: \hspace{1em} An affine linear form in $\set{x_1, x_2, \ldots , x_n}$ \pause
		\item Polynomial computed by the path $p$ = $\mathsf{wt}(p)$: \hspace{1em} Product of the edge labels on $p$ \pause
		\item Polynomial computed by the ABP: \hspace{1em} $f_{\abp}(\vecx) = \sum_p \mathsf{wt}(p)$ 
	\end{itemize}
\end{frame}

\begin{frame}{Lower Bounds in Algebraic Circuit Complexity}

	\begin{center}
		\uncover<1->{\textbf{Objects of Study}: Polynomials over $n$ variables of degree $d$.}
	\end{center}

	\begin{columns}
		\begin{column}{.65\textwidth}
			
			\vspace{.75em}
			\uncover<3->{$\VF$: Polynomials computable by formulas of size $\poly(n,d)$.}

			\vspace{.75em}
			\uncover<4->{$\VBP$: Polynomials computable by ABPs of size $\poly(n,d)$.}

			\vspace{.75em}
			\uncover<2->{$\VP$: Polynomials computable by circuits of size $\poly(n,d)$.}

			\vspace{.75em}
			\uncover<5->{$\VNP$: Explicit Polynomials}

			\vspace{1em}
			\begin{center}
				\uncover<6->{Are the inclusions tight?}
			\end{center}
		\end{column}
		\begin{column}{.35\textwidth}

			\vspace{1em}
			\begin{tikzpicture}[scale=1.1,transform shape]
				\uncover<5->{\node at (1.3,-2.1) {$\VNP$};
				\node[rectangle,text width=3.5cm,text height=3.5cm,draw] at (0,-0.6) (vnp) {};}
				\uncover<2->{\node[set,text width=3cm,label={[below=70pt of vp]$\VP$}] (vf) at (0,-0.4)  (vp) {};}
				\uncover<4->{\node[set,text width=2cm,label={[below=40pt of vbp]$\VBP$}] (vbp) at (0,-0.2)  {};}
				\uncover<3->{\node[set,text width=1cm] (vf) at (0,0) {$\VF$};}
			\end{tikzpicture}
		\end{column}
	\end{columns}

	\vspace{1.2em}
	\uncover<7->{\textbf{Central Question}: Find \textcolor{BrickRed}{explicit} polynomials that cannot be computed by \textcolor{MidnightBlue}{efficient} circuits.}
	\vspace{-2em}
\end{frame}

\begin{frame}{Some Natural Restrictions}
	\begin{columns}
		\begin{column}{.25\textwidth}
			\uncover<2->{\begin{center}
				\textbf{Homogenity}\\
				Every monomial is of the same degree.
			\end{center}}
		\end{column}
		\begin{column}{.425\textwidth}
			\[
				\boxed{\Det{n}(\vecx) = \sum_{\sigma \in S_n} (-1)^{\sgn(\sigma)} \prod_{i=1}^{n} x_{i \sigma(i)}}	
			\]
		\end{column}
		\begin{column}{.325\textwidth}
			\uncover<4->{\begin{center}
				\textbf{Multilinearity}\\
				No variable occurs more than once in any monomial.
			\end{center}}
		\end{column}
	\end{columns}

	\vspace{1.5em}
	\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{center}
				\uncover<3->{\textbf{Homogeneous Circuits}\\
				Every gate computes a homogeneous polynomial.}
			
				\vspace{2em}
				\uncover<6->{\textbf{Non-Commutative Circuits}\\
				The multiplication gates are non-commutative.}
			\end{center}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{center}
				\uncover<5->{\textbf{Multilinear Circuits}\\
				Every gate computes a multilinear polynomial.}
			
				\vspace{2em}
				\uncover<7->{\textbf{Constant Depth Circuits}\\
				Length of the longest root-to-leaf path is a constant independent of $n$.}
			\end{center}
		\end{column}
	\end{columns}
	\vspace{-1.5em}
\end{frame}

\begin{frame}{The Great Success: Constant Depth Circuits}
	\begin{center}
		\textbf{[Valiant-Skyum-Berkowitz-Rackoff]}
		
		Efficient circuits can be converted into efficient circuits of \textcolor{ForestGreen}{depth $O(\log d)$}.
	\end{center}\pause

	\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{center}
				\textbf{[Agrawal-Vinay, Koiran, Tavenas]}
				
				Size $s$ circuits computing $n$-variate degree $d$ polynomials can be converted into \textcolor{ForestGreen}{depth-$4$} circuits of \textcolor{MidnightBlue}{size $s^{O(\sqrt{d})}$}.
			\end{center}\pause
			\begin{center}
				\textbf{[Gupta-Kamath-Kayal-Saptharishi]}

				Size $s$ circuits computing $n$-variate degree $d$ polynomials can be converted into \textcolor{ForestGreen}{depth-$3$} circuits of size $s^{O(\sqrt{d})}$.
			\end{center}
		\end{column}\pause
		\begin{column}{.5\textwidth}
			\begin{center}
				\textbf{[Limaye-Srinivasan-Tavenas]}
				
				$\imm{n}{\log n}(\vecx)$ \textcolor{BrickRed}{can not be computed} by \textcolor{ForestGreen}{constant depth} circuits of \textcolor{MidnightBlue}{size $\poly(n)$}.
			\end{center}\pause
			\begin{center}
				The \textcolor{BrickRed}{lower bound} is \textcolor{MidnightBlue}{$n^{\Omega(\sqrt{d})}$} for \textcolor{ForestGreen}{depth-$3$ and depth-$4$}, proving that the depth reduction statements are tight.
			\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Successes in Other Restricted Models}
	\begin{center}
		\textbf{Multilinear Setting}
		
		\textbf{[Dvir-Malod-Perifel-Yehudayoff]}: There is an explicit polynomial that is computable by efficient multilinear ABPs but not by any efficient multilinear formula.
	\end{center} \pause

	\vspace{.2em}
	\begin{center}
		\textbf{Non-Commutative Setting}

		\textbf{[Nisan]}: There is an explicit non-commutative polynomial that is computable by efficient circuits but not by any efficient ABP. \pause

		\vspace{.25em}
		\textbf{[Limaye-Srinivasan-Tavenas]}: There is an explicit non-commutative polynomial that is computable by homogeneous ABPs but not by any efficient homogeneous formula.  \pause

		\vspace{.25em}
		\textbf{[\textcolor{BrickRed}{Cha}]}: There is a tight separation between ABPs and some syntactically structured formulas.
	\end{center}
	\vspace{-1.5em}
\end{frame}

\begin{frame}{Lower Bounds for General Models}
	\begin{center}
		\textbf{General Circuits}

		\textbf{[Baur-Strassen]}: Any algebraic circuit computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(n \log d)$ wires. \pause
	\end{center}
	
	\vspace{.75em}
	\begin{center}
		\textbf{Homogeneous ABPs} 
		
		\textbf{[Kumar]}: Any ABP with $(d+1)$ layers computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ vertices.\pause
	\end{center}
		
	\vspace{.75em}	
	\begin{center}
		\textbf{General ABPs} 
		
		\textbf{[\textcolor{BrickRed}{C}-Kumar-She-Volk]}: Any ABP computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ vertices.
	\end{center}	
	\vspace{-1em}	
\end{frame}

\begin{frame}{Lower Bounds for General Models (contd.)}
	\begin{center}
		\textbf{General Formulas}

		\vspace{.5em}
		\textbf{[Kalorkoti]}: Any formula computing the $n^2$-variate $\Det{n}(\vecx)$ requires $\Omega(n^3)$ wires.\pause

		\vspace{1em}
		\textbf{[Shpilka-Yehudayoff]} (using Kalorkoti's method): There is an $n$-variate multilinear polynomial such that any formula computing it requires $\Omega(n^2/\log n)$ wires.\pause

		\vspace{1.5em}
		\textbf{[\textcolor{BrickRed}{C}-Kumar-She-Volk]}: Any formula computing $\mathsf{ESym}_{n,0.1n}(\vecx)$ requires $\Omega(n^2)$ vertices, where
		\[
			\esym{n}{d}(\vecx) = \sum_{i_1 < \cdots < i_d \in [n]} x_{i_1} \cdots x_{i_d}.
		\]
	\end{center}
	\vspace{-2em}
\end{frame}

\begin{frame}{Natural Proofs}
	\begin{center}
		Are the proof techniques used against structured models useful against general models?

		\vspace{.25em}
		\uncover<2->{\textbf{[Forbes-Shpilka-Volk, Grochow-Kumar-Saks-Saraf]}: Defined \textcolor{MidnightBlue}{Algebraically Natural Proofs}.}
	\end{center}

	\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{center}      		
				\begin{tikzpicture}[transform shape, scale=0.85]
			        \uncover<3->{
			          \draw (-3.75,-1.75) rectangle (3.75,1.75);
			          \node (labelU1) at (2.8,1.4) {Set of all}; 
			          \node (labelU2) at (2.6,.9) {polynomials};
			          \draw[color=NavyBlue, thick] (-2,0) circle (30pt);
			          \node[color=NavyBlue] (labelC) at (-2,0) {$\ckt$};
			        }
			        %\pause
			        \uncover<5->{
			          \draw[color=Cyan, thick] (-1.2,0) ellipse (60pt and 35pt);
			          \node[color=Cyan] (labelD) at (2,-1) {Explicit polynomials};
			        }
			        \uncover<4->{
			          \draw[color=Green, thick, fill=Green, opacity=0.25] (-2.1,0) ellipse (36pt and 45pt);
			          \node[color=Green] (labelP) at (-0.3,1.45) {$\calp : P(\bar{f})=0$};
			        }
	      		\end{tikzpicture}
      		\end{center}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{center}
			\uncover<6->{\textbf{[\textcolor{BrickRed}{C}-Kumar-Ramya-Saptharishi-Tengse]}:\\

			Let $\VP'$ be the polynomials in $\VP$ that additionally have $\set{-1,0,1}$ coefficients. 

			Then, $\VP'$ has $\VP$ natural proofs.}
			\end{center}
		\end{column}
	\end{columns}

	\vspace{1em}
	\uncover<7->{\textbf{[K-R-S-T]}: Suppose the Permanent polynomial is $2^{n^{\epsilon}} $-hard for constant $\epsilon > 0$. 
	In this case, if $\VP$ has natural proofs, then there is also a natural proof $P$ that has \textcolor{ForestGreen}{explicit non-roots}.}
\end{frame}

\begin{frame}{There is So Much Left To Do}
	\begin{center}
		We have made great progress in this field.
		But there is still a lot that we do not know.
	\end{center}\pause

	\vspace{1em}
	\textbf{Some Concrete Questions I Have Been Thinking About...}
	\begin{itemize}
		\item Super-quadratic lower bound against homogeneous formulas?\pause
		\item Super-quadratic lower bounds against (homogeneous) multilinear ABPs?\pause
		\item Separation between ABPs and formulas in the non-commutative setting?\pause
		\item Does $\VP$ have natural proofs? Maybe under some natural assumptions?
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{center}
		\large{\textbf{Questions?}}
	\end{center}
\end{frame}

\begin{frame}{Algebraic Branching Programs}
	\begin{center}
	\begin{tikzpicture}[thick, node distance=2cm]
		\hspace{-.5em}
		\node[circle, draw=black] (start) {$s$};
		\node[circle, draw=black] (l11) at ($(start)+(2.6,1.5)$) {};
		\node[circle, draw=black] (l12) at ($(start)+(2.6,-.5)$) {};
		\node[circle, draw=black] (l21) at ($(start)+(5.2,0)$) {};
		\node[circle, draw=black] (l31) at ($(start)+(7.8,1.5)$) {};
		\node[circle, draw=black] (l32) at ($(start)+(7.8,-.5)$) {};
		\node[circle, draw=black] (l41) at ($(start)+(10.4,1.5)$) {};
		\node[circle, draw=black] (l42) at ($(start)+(10.4,-.5)$) {};
		\node[circle, draw=black] (end) at ($(start)+(13,1)$) {t};
		
		\draw[->](start) -- (l11);
		\only<2-4>{\draw[->](start) -- node[left] {\small{$(2x+3)$}} (l11)};
		\only<3>{\textcolor{red}{\draw[->](start) -- node[left] {\small{\textcolor{black}{$(2x+3)$}}} (l11);}}
		\draw[->](start) -- (l12);
		\draw[->](l11) -- (l21);
		\only<2-4>{\draw[->](l11) -- node[left] {\small{$(x+3y)$}} (l21)};
		\only<3>{\textcolor{red}{\draw[->](l11) -- node[left] {\small{\textcolor{black}{$(x+3y)$}}} (l21);}}
		\draw[->](l12) -- (l21);
		\draw[->](l21) -- (l31);
		\only<2-4>{\draw[->](l21) -- node[left] {\small{$(y+5)$}} (l31)};
		\only<3>{\textcolor{red}{\draw[->](l21) -- node[left] {\small{\textcolor{black}{$(y+5)$}}}(l31);}}
		\draw[->](l21) -- (l32);
		\draw[->](l31) -- (l42);
		\only<2,4>{\draw[->](l31) -- node[left] {\small{$10$}} (l42)};
		\only<3>{\textcolor{red}{\draw[->](l31) -- node[left] {\small{\textcolor{black}{$10$}}}(l42);}}
		\draw[->](l32) -- (l41);
		\draw[->](l32) -- (l42);
		\draw[->](l41) -- (end);
		\draw[->](l42) -- (end);
		\only<2-4>{\draw[->](l42) -- node[left] {\small{$(x+y+7)$}} (end)};
		\only<3>{\textcolor{red}{\draw[->](l42) -- node[left] {\small{\textcolor{black}{$(x+y+7)$}}} (end);}}

		\node at ($(start)+(13,-.25)$) {$\abp$};
	\end{tikzpicture}
	\end{center}\pause

	\vspace{.75em}
	\begin{itemize}
		\item Label on each edge: \hspace{1em} An affine linear form in $\set{x_1, x_2, \ldots , x_n}$ \pause
		\item Polynomial computed by the path $p$ = $\mathsf{wt}(p)$: \hspace{1em} Product of the edge labels on $p$ \pause
		\item Polynomial computed by the ABP: \hspace{1em} $f_{\abp}(\vecx) = \sum_p \mathsf{wt}(p)$ \pause
	\end{itemize}

	\vspace{1em}
	\begin{center}
		\textbf{Question}: Is there an \textcolor{BrickRed}{explicit} polynomial that can not be computed by \textcolor{MidnightBlue}{efficient} ABPs?
	\end{center}
	\vspace{-2em}
\end{frame}

\begin{frame}{Lower Bounds Against ABPs}
	\begin{center}
		\textbf{Previous Work}

		\vspace{.5em}
		\textbf{[Baur-Strassen]}: Any ABP computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(n \log d)$ \only<1-2>{wires}\only<3>{\textcolor{BrickRed}{wires}}.\\ \pause
	
		\vspace{1em}
		\textbf{[Kumar]}: Any ABP with $(d+1)$ layers computing $\sum_{i=1}^{n} x_i^d$ has $\Omega(nd)$ vertices. \pause
	\end{center}

	\vspace{1em}
	\begin{center}
		\textbf{Our Result}

		\vspace{.5em}
		\textbf{[\textcolor{BrickRed}{C}-Kumar-She-Volk]}: Any ABP computing $\sum_{i=1}^{n} x_i^d$ requires $\Omega(nd)$ \textcolor{BrickRed}{vertices}.
	\end{center}
\end{frame}

\begin{frame}{The Homogeneous Case: Proof Overview}
	\begin{center}
		\begin{tikzpicture}[thick, node distance=2cm, scale=0.75]
			\node[circle, draw=black] (start) {$s$};
			\node[circle, draw=black] (l11) at ($(start)+(2,1)$) {};
			\node[circle, draw=black] (l12) at ($(start)+(2,-1)$) {};
			\node[circle, draw=black] (l21) at ($(start)+(4,0)$) {};
			\node[circle, draw=black] (l31) at ($(start)+(6,1)$) {};
			\node[circle, draw=black] (l32) at ($(start)+(6,-1)$) {};
			\node[circle, draw=black] (l41) at ($(start)+(8,1)$) {};
			\node[circle, draw=black] (l42) at ($(start)+(8,-1)$) {};
			\node[circle, draw=black] (end) at ($(start)+(10,0)$) {t};
			
			\draw[->](start) -- (l11);
			\draw[->](start) -- (l12);
			\draw[->](l11) -- (l21);
			\draw[->](l12) -- (l21);
			\draw[->](l21) -- (l31);
			\draw[->](l21) -- (l32);
			\draw[->](l31) -- (l42);
			\draw[->](l32) -- (l41);
			\draw[->](l32) -- (l42);
			\draw[->](l41) -- (end);
			\draw[->](l42) -- (end);
		\end{tikzpicture}
		\hspace{1em}
		$P_{n,d}(\vecx) = \sum_{i=1}^{n} x_i^d$.
	\end{center}\pause
	
	\vspace{-1em}
	\[
		P_{n,d} = \sum_{i=1}^{t_k} [s,v_i] \cdot [v_i, t] \pause = \sum_{i=1}^{t_k} g_i \cdot h_i \pause = \sum_{i=1}^{t_k} (g'_i + \alpha_i) \cdot (h'_i + \beta_i)
	\] \pause
	
	\vspace{-1.5em}
	\[
		\text{For } R = \sum_{i=1}^{t_k} (\alpha_i \cdot h'_i + \beta_i \cdot g'_i + \alpha_i \cdot \beta_i) \text{, \hspace{1em}}\pause
		P_{n,d} = \inparen{\sum_{i=1}^{t_k} g'_i \cdot h'_i} + R \pause \implies \textcolor{MidnightBlue}{P_{n,d} - R = \sum_{i=1}^{t_k} g'_i \cdot h'_i}. \pause
	\]

	\vspace{-1.5em}
	\begin{center}
		\textbf{Note}: $\deg(g'_i), \deg(h'_i) \leq [d-1]$ for every $i \in [d-1]$.
	 	Thus $\deg(R) \leq d-1$.
	\end{center}
	\vspace{-1.5em}
\end{frame}

\begin{frame}{The Homogeneous Case: Proof Overview (contd.)}
	\begin{center}
		$P_{n,d} - R = \sum_{i=1}^{t_k} g'_i \cdot h'_i$ \hspace{5em} where \hspace{5em} $P_{n,d} = \sum_{i=1}^{n} x_i^d$
	\end{center}\pause 
	\begin{center}
		Look at the first order derivatives.
	\end{center}
	\vspace{-1em}
	\begin{center}
		$S = \set{\partial_{x_i}(P_{n,d}-R)} = \set{d \cdot x_i^{d-1} - \partial_{x_i}(R)}_{i \in [n]}$\pause \hspace{2em}
		$S' = \set{\sum_{j=1}^{t_k} (g'_j \cdot \partial_{x_i}h'_j + h'_j \cdot \partial_{x_i} g'_j)}_{i \in [n]}$\pause
	\end{center}
	\vspace{-1em}
	\begin{center}
		$\mathcal{V} = $ Set of common zeroes of $S$ and $S'$ \pause \hspace{3em} 
		$\mathcal{V}' = $ Set of common zeroes of $\set{g'_j, h'_j}_{j \in [t_k]}$ \pause
	\end{center}
	\vspace{1em}
	\[
		\mathcal{V}' \subseteq \mathcal{V} \pause \implies \textcolor{MidnightBlue}{\dim(\mathcal{V}') \leq \dim(\mathcal{V})} \pause
	\]
	\vspace{-1em}
	\only<1-10>{\[R = \zero \pause \hspace{1em} \Rightarrow \hspace{1em} S = \set{d \cdot x^{d-1}_1, \ldots, d \cdot x^{d-1}_n} \pause \hspace{.75em} \Rightarrow \hspace{.75em} \textcolor{MidnightBlue}{\dim(\mathcal{V}) = \zero} \pause\]}
	\only<11->{\[\deg(R) \leq d-1 \hspace{1em} \implies \hspace{1em} \textcolor{MidnightBlue}{\dim(\mathcal{V}) = \zero} \pause \]}
	\pause \pause \pause
	\vspace{-1em}
	\[
		\textcolor{ForestGreen}{\dim(\mathcal{V}') \geq n-2t_k} \pause \implies n-2t_k \leq \zero \pause \implies \textcolor{ForestGreen}{t_k \geq n/2}
	\]
	\vspace{-3em}
\end{frame}

\begin{frame}{Proof Overview Of Our Result}
	\textbf{Step 0} ([Kumar]): \underline{Look at the homogeneous case}
	\begin{center}
		Any ABP with $(d+1)$ layers computing $\sum_{i=1}^{n} x_i^d$ has $\Omega(nd)$ vertices.\pause
	\end{center}

	\vspace{1em}
	\textbf{Step 1}: \underline{Generalise above statement to get the base case}\\
	\begin{center}
		Any ABP with $(d+1)$ layers \pause computing a polynomial of the form 
	\[
		f = \sum_{i=1}^n x_i^d + \sum_{i=1}^r A_i(\vecx) \cdot B_i(\vecx) + \delta(\vecx)
	\] \pause
	where \hspace{1.5em} $A_i(\zero) = \zero = B_i(\zero)$ \hspace{1.5em} and \hspace{1.5em} $\deg(\delta(\vecx)) < d$, \hspace{1.5em} \pause has at least 
	\[
		\text{\textcolor{BrickRed}{$((n/2) - r) \cdot (d-1)$} \hspace{1em} vertices}.
	\]
	\end{center}
	\vspace{-3em}
\end{frame}

\begin{frame}{Proof Overview Of Our Result (contd.)}
	\textbf{Step 2}: \underline{Iteratively reduce to Base Case}\\
	\vspace{.5em}
	In each iteration, reduce the number of layers till it becomes $(d+1)$ such that\pause
	\vspace{.5em}
	\begin{itemize}
		\item the number of layers is reduced by a constant fraction,\pause
		\item the size does not increase, \pause
		\item the polynomial being computed continues to look like
		\[
			f_{\ell +1} = \sum_{i=1}^{n} x_i^d + \sum_{i=1}^{r_{\ell+1}} A_i(\vecx) \cdot B_i(\vecx) + \delta_{\ell +1}(\vecx)
		\]
		where \hspace{1em} $A_i(\zero) = \zero = B_i(\zero)$ \hspace{1em} and \hspace{1em} $\deg(\delta_{\ell + 1}(\vecx)) < d$, \pause
		\item number of error terms collected is small. 
	\end{itemize}
\end{frame}

\begin{frame}{The Induction Step}
	\underline{$\ell$-th step}\\ \pause
	\begin{columns}
		\begin{column}{.05\textwidth}
		\end{column}
		\begin{column}{.3\textwidth}
			\begin{center}
				\textbf{Given}: $\mathcal{A}_\ell$
			\end{center}
			Size = $s_\ell$\\
			Number of layers = $d_\ell$\\
			Number of error terms = $r_\ell$\\ \pause
		\end{column}
		\begin{column}{.1\textwidth}
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{center}
				\textbf{Want to construct}: $\mathcal{A}_{\ell + 1}$
			\end{center}
			Size = $s_{\ell+1} \pause \leq s_\ell$ \pause\\
			Number of layers = $d_{\ell + 1} \pause \leq \dfrac{2}{3} d_\ell$\\ \pause
			Number of error terms = $r_{\ell + 1} \pause \leq r_\ell + \dfrac{s_\ell}{d_\ell/3}$\pause
		\end{column}
		\begin{column}{.05\textwidth}
		\end{column}
	\end{columns}

	\vspace{2.5em}
	\textbf{Number of Steps}: $\Theta(\log n)$ \hspace{4em}\pause
	\textbf{Number of Error Terms}: $\epsilon \cdot n$ where $\epsilon$ can be chosen.\pause

	\vspace{.5em}
	\begin{center}
		\textbf{The Lower Bound}:  $((n/2) - \epsilon \cdot n) \cdot (d-1)$
	\end{center}
	\vspace{-2.5em}
\end{frame}

\begin{frame}{Proof of the Induction Step}
	\vspace{1em}
	\uncover<3-8,9-11>{$\mathcal{A_\ell} = f_1 \cdot f_2$}\\
	\vspace{.5em}
	\begin{center}
		\begin{tikzpicture}[thick, node distance=2cm]
			\node[circle, draw=black] (start) {$s$};
			\node[circle, draw=black] (l1) at ($(start)+(3,0)$) {};
			\node[circle, draw=black] (l2) at ($(start)+(5,0)$) {};
			\node[circle, draw=black] (l3) at ($(start)+(7,0)$) {};
			\node[circle, draw=black] (end) at ($(start)+(10,0)$) {t};
			
			\draw[->, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](start) -- (l1);
			\draw[->] (l1) -- node[above] {$a_1$} (l2);
			\draw[->] (l2) -- node[above] {$a_2$} (l3);
			\draw[->, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](l3) -- (end);
	
			\only<11>{
				\draw[->, MidnightBlue, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](start) -- (l1);
				\node[circle, draw=MidnightBlue] (l1) at ($(start)+(3,0)$) {};
				\draw[->, MidnightBlue] (l1) -- node[above] {$a_1$} (l2);
				\draw[->, ForestGreen] (l2) -- node[above] {$a_2$} (l3);
				\node[circle, draw=ForestGreen] (l3) at ($(start)+(7,0)$) {};
				\draw[->, ForestGreen, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](l3) -- (end);
			}
			
			\uncover<2-8>{\node (f1) at ($(start)+(2.5,1)$) {$f_1 = f'_1 + \alpha$};
			\draw[->] ($(start)+(1.25,1)$) -- ($(start)+(0,1)$);
			\draw[->] ($(start)+(3.74,1)$) -- ($(l2)+(-0.1,1)$);
			
			\node (f2) at ($(end)+(-2.5,1)$) {$f_2 = f'_2 + \beta$};
			\draw[->] ($(end)+(-1.25,1)$) -- ($(end)+(0,1)$);
			\draw[->] ($(end)+(-3.74,1)$) -- ($(l2)+(0.1,1)$);}
		\end{tikzpicture}
	\end{center}
	
	\vspace{2em}
	\uncover<7-8,9-11>{$\mathcal{A}_{{\ell}+1} = \beta \cdot f_1 + \alpha \cdot f_2$} 
	\uncover<8>{$= \mathcal{A}_\ell - f'_1 \cdot f'_2 + \alpha \cdot \beta$}\\
	
	\begin{center}
		\only<1-9>{\begin{tikzpicture}[thick, node distance=2cm]
		\uncover<4->{\node[circle, draw=black] (start) {$s$};
		\node[circle, draw=black] (l1) at ($(start)+(3,0)$) {};
		\node[circle, draw=black] (l21) at ($(start)+(5,.5)$) {};
		\node[circle, draw=black] (l22) at ($(start)+(5,-.5)$) {};
		\node[circle, draw=black] (l3) at ($(start)+(7,0)$) {};
		\node[circle, draw=black] (end) at ($(start)+(10,0)$) {t};}
		
		\uncover<4->{\draw[->, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](start) -- (l1);}
		\uncover<5->{\draw[->] (l1) -- node[above] {$a_1$}(l21);
		\draw[->] (l21) to [out=10,in=160] node[above] {$\beta$}(end);}
		\uncover<6->{\draw[->] (l22) -- node[below] {$a_2$}(l3);
		\draw[->] (start) to [out=340,in=200] node[above] {$\alpha$}(l22);}
		\uncover<4->{\draw[->, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](l3) -- (end);}	
		\end{tikzpicture}}
	
		\only<10>{
			\begin{tikzpicture}[thick, node distance=2cm]
				\node[circle, draw=black] (start) {$s$};
				\node[circle, draw=black] (l1) at ($(start)+(3,0)$) {};
				\node[circle, draw=black] (l3) at ($(start)+(7,0)$) {};
				\node[circle, draw=black] (end) at ($(start)+(10,0)$) {t};
				
				\draw[->, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](start) -- (l1);
				\draw[->] (l1) to [out=10,in=160] node[above] {$\beta \cdot a_1$}(end);
				\draw[->] (start) to [out=340,in=200] node[above] {$\alpha \cdot a_2$}(l3);
				\draw[->, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](l3) -- (end);	
			\end{tikzpicture}
		}
		\only<11>
		{	
			\begin{tikzpicture}[thick, node distance=2cm]
				\node[circle, draw=black] (start) {$s$};
				\draw[->, MidnightBlue, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](start) -- (l1);
				\node[circle, draw=MidnightBlue] (l1) at ($(start)+(3,0)$) {};
				\draw[->, MidnightBlue] (l1) to [out=10,in=160] node[above] {$\beta \cdot a_1$}(end);
				\draw[->, ForestGreen] (start) to [out=340,in=200] node[above] {$\alpha \cdot a_2$}(l3);
				\node[circle, draw=ForestGreen] (l3) at ($(start)+(7,0)$) {};	
				\node[circle, draw=black] (end) at ($(start)+(10,0)$) {t};
				\draw[->, ForestGreen, decorate, decoration={zigzag, segment length=10, amplitude=2,post=lineto, post length=2pt}](l3) -- (end);
			\end{tikzpicture}
		}
	\end{center}
\end{frame}

\begin{frame}{Algebraic Formulas And Lower Bounds Against Them}
	\begin{columns}
		\begin{column}{.4\textwidth}
			\[
				x^3 + 3x^2y + 3xy^2 + y^3 = (x+y)^3
			\]

			\vspace{-1em}
			\only<1>{\[
				\text{\textcolor{BrickRed}{$(x+y) \cdot (x+y) \cdot (x+y)$}}
			\]}
			\only<2->{\[
				\text{$(x+y) \cdot (x+y) \cdot (x+y)$}
			\]}

			\begin{center}
				\begin{tikzpicture}[thick, node distance=1cm]
					\draw
					node [name=dummy] {}
					node [sum, right of=dummy] (top) {\proda}
					node [sum] at ($(top)+(-1.5,-1.5)$) (sum1) {\suma}
					node [sum] at ($(top)+(0,-1.5)$) (sum2) {\suma}
					node [sum] at ($(top)+(1.5,-1.5)$) (sum3) {\suma};
					\node (var1) at ($(top)+(-1.8,-3)$) {$x$};
					\node (var2) at ($(top)+(-1.2,-3)$) {$y$};
					\node (var3) at ($(top)+(-0.3,-3)$) {$x$};
					\node (var4) at ($(top)+(0.3,-3)$) {$y$};
					\node (var5) at ($(top)+(1.2,-3)$) {$x$};
					\node (var6) at ($(top)+(1.8,-3)$) {$y$};
					
					\draw[->](sum1) -- (top);
					\draw[->](sum2) -- (top);
					\draw[->](sum3) -- (top);

					\draw[->](var1) -- (sum1);
					\draw[->](var2) -- (sum1);
					\draw[->](var3) -- (sum2);
					\draw[->](var4) -- (sum2);
					\draw[->](var5) -- (sum3);
					\draw[->](var6) -- (sum3);
				\end{tikzpicture}
			\end{center}
		\end{column} \pause

		\begin{column}{.6\textwidth}
			\textbf{[Kalorkoti]}: Any formula computing $\Det{n\times n}(\vecx)$ has atleast $\Omega(n^3)$ \textcolor{BrickRed}{wires}.\\ \pause
			\vspace{.5em}
			\textbf{[Shpilka, Yehudayoff]} (using Kalorkoti's method):\\
			There is a multilinear polynomial such that any formula computing it requires $\Omega(n^2/\log n)$ \textcolor{BrickRed}{wires}.\\ \pause
			\vspace{1em}
			\textbf{Our Result}: Any formula computing $\esym{n}{0.1n}(\vecx)$ has atleast $\Omega(n^2)$ \textcolor{BrickRed}{vertices}, where
			\[
				\esym{n}{0.1n}(\vecx) = \sum_{i_1 < \cdots < i_{0.1n} \in [n]} \hspace{.5em} \prod_{j=1}^{0.1n} x_{i_j}.
			\]
		\end{column}
	\end{columns}	
	\vspace{-2em}
\end{frame}

\begin{frame}{Some Subtelties: Why Multilinear?}
	\begin{center}
		[Shpilka-Yehudayoff]: Any formula computing $\sum_{i=1}^{n} \sum_{j=1}^{n} x_i^j y_j$ requires $\Omega(n^2)$ wires.	\pause
	\end{center}

	\vspace{.5em}
	\textbf{Why is Our Result Interesting Then?}
	\begin{itemize}
		\item This is trivial to show since there are $n$ variables that have \emph{individual degree} $n$.\pause
		\item Interesting only when the bound is $\omega(\sum_{i \in [n]} d_i)$ when $d_i$ is the individual degree of $x_i$.\pause
	\end{itemize}

	\vspace{1em}
	\textbf{Multilinear Polynomials Are An Interesting Subclass}
	\begin{itemize}
		\item Individual Degree of every variable is $1$.\pause
		\item Multilinearisation of the SY polynomial gives an $\Omega(n^2/\log n)$ lower bound.\pause
		\item Kalorkoti's method can not give a better bound against multilinear polynomials.
	\end{itemize}	
\end{frame}

\begin{frame}{Conclusion}
	\begin{center}
		\vspace{5em}
		\textbf{\large{Proving Lower Bounds is Cool!}}\\
		\vspace{1em}
		You should do it too... :)\pause
	
		\vspace{5em}
		\textbf{\large{Thank You !}}\\
		\vspace{1.5em}
		Webpage: preronac.bitbucket.io \hspace{5em} Email: prerona.ch@gmail.com
	\end{center}
\end{frame}
\end{document}